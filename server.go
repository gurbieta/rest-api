package main

import (
	"database/sql"
	"log"
	"net/http"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/gorp.v1"
)

// User data
type User struct {
	ID        int64     `db:"id" json:"id"`
	Firstname string    `db:"firstname" json:"firstname"`
	Lastname  string    `db:"lastname" json:"lastname"`
	Email     string    `db:"email" json:"email"`
	Created   time.Time `db:"created" json:"created"`
	Updated   time.Time `db:"modified" json"modified"`
}

// Login data
type login struct {
	User     string `form:"user" json:"user" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func main() {
	GetRouter().Run(":8080")
}

// GetRouter Returns Gin router
func GetRouter() *gin.Engine {
	r := gin.Default()

	r.Use(cors())

	users := r.Group("users")
	{
		users.GET("", getUsers)
		users.GET("/:id", getUser)
		users.POST("", postUser)
		users.PUT("/:id", updateUser)
		users.DELETE("/:id", deleteUser)

		users.OPTIONS("", optionsUser)     // POST
		users.OPTIONS("/:id", optionsUser) // PUT, DELETE
	}

	return r
}

func validateUser(user *User) []string {
	fields := []string{}
	if user.Firstname == "" {
		fields = append(fields, "firstname")
	}
	if user.Lastname == "" {
		fields = append(fields, "lastname")
	}
	if user.Email == "" {
		fields = append(fields, "email")
	}

	return fields
}

func getUsers(c *gin.Context) {
	var users []User
	_, err := dbmap.Select(&users, "SELECT * FROM user")
	if err == nil {
		c.JSON(http.StatusOK, users)
	} else {
		c.JSON(http.StatusNotFound, gin.H{"error": "no user(s) into the table"})
	}
}

func getUser(c *gin.Context) {
	id := c.Params.ByName("id")
	user, err := dbmap.Get(User{}, id)
	if err == nil {
		if user == nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "user [" + id + "] not found"})
		} else {
			c.JSON(http.StatusOK, user)
		}
	} else {
		checkErr(err, "Can't get user ["+id+"]")
	}

}

func postUser(c *gin.Context) {
	var user User
	c.Bind(&user)
	var fields = validateUser(&user)

	if len(fields) > 0 {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "fields are empty: " + strings.Join(fields, ", ")})
	} else {
		user.Created = time.Now()
		user.Updated = time.Now()
		err := dbmap.Insert(&user)
		if err == nil {
			c.JSON(http.StatusOK, user)
		} else {
			checkErr(err, "Insert failed")
		}
	}
}

func updateUser(c *gin.Context) {
	id := c.Params.ByName("id")
	iface, err := dbmap.Get(User{}, id)
	if err == nil {
		if iface == nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "user [" + id + "] not found"})
		} else {
			user := iface.(*User)
			c.Bind(&user)

			var fields = validateUser(user)

			if len(fields) > 0 {
				c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "fields are empty: " + strings.Join(fields, ", ")})
			} else {
				user.Updated = time.Now()
				_, err = dbmap.Update(user)
				if err == nil {
					c.JSON(http.StatusOK, user)
				} else {
					checkErr(err, "Updated failed")
				}
			}
		}
	} else {
		checkErr(err, "Can't update user ["+id+"]")
	}
}

func deleteUser(c *gin.Context) {
	id := c.Params.ByName("id")
	iface, err := dbmap.Get(User{}, id)
	if err == nil {
		if iface == nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "user [" + id + "] not found"})
		} else {
			user := iface.(*User)
			_, err = dbmap.Delete(user)
			if err == nil {
				c.JSON(http.StatusNoContent, gin.H{"msg": "user [" + id + "] deleted"})
			} else {
				checkErr(err, "Delete failed")
			}
		}
	} else {
		checkErr(err, "Can't delete user ["+id+"]")
	}
}

func cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func optionsUser(c *gin.Context) {
	c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "DELETE,POST,PUT")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	c.Next()
}

var dbmap = initDb()

func initDb() *gorp.DbMap {
	db, err := sql.Open("mysql", "root:123456@/myapi?parseTime=true")
	checkErr(err, "sql.Open failed")
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	dbmap.AddTableWithName(User{}, "user").SetKeys(true, "ID")
	err = dbmap.CreateTablesIfNotExists()
	checkErr(err, "Create table failed")
	return dbmap
}

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}
