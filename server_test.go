package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var userID int64

func getJSON(res *httptest.ResponseRecorder, target interface{}) error {
	return json.NewDecoder(res.Body).Decode(target)
}

func getJSONMap(res *httptest.ResponseRecorder) map[string]string {
	jsonObj := map[string]string{}
	getJSON(res, &jsonObj)
	return jsonObj
}

func getUserID() string {
	return strconv.FormatInt(userID, 10)
}

func checkErrTest(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func makeRequest(method string, url string, jsonObj string) (*http.Request, *httptest.ResponseRecorder) {
	router := GetRouter()

	var reader io.Reader

	if jsonObj != "" {
		reader = strings.NewReader(jsonObj)
	}

	req, err := http.NewRequest(method, url, reader)
	checkErrTest(err)

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res := httptest.NewRecorder()
	router.ServeHTTP(res, req)

	return req, res
}

func TestPostUser(t *testing.T) {
	_, res := makeRequest("POST", "/users", `{ "firstname": "Thea", "lastname": "Queen", "email": "thea@queen.com" }`)

	assert.Equal(t, http.StatusOK, res.Code)

	var user User
	getJSON(res, &user)

	assert.Equal(t, "Thea", user.Firstname)
	assert.Equal(t, "Queen", user.Lastname)
	assert.Equal(t, "thea@queen.com", user.Email)
	assert.NotNil(t, user.Created)
	assert.NotNil(t, user.Updated)

	userID = user.ID
}

func TestPostEmptyUser(t *testing.T) {
	_, res := makeRequest("POST", "/users", `{}`)

	assert.Equal(t, http.StatusUnprocessableEntity, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "fields are empty: firstname, lastname, email", jsonObj["error"])
}

func TestUpdateInexistentUser(t *testing.T) {
	_, res := makeRequest("PUT", "/users/0", `{ "firstname": "Thea", "lastname": "Queen", "email": "thea@queen.com" }`)

	assert.Equal(t, http.StatusNotFound, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "user [0] not found", jsonObj["error"])
}

func TestUpdateEmptyUser(t *testing.T) {
	_, res := makeRequest("PUT", "/users/"+getUserID(), `{ "firstname": "" }`)

	assert.Equal(t, http.StatusUnprocessableEntity, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "fields are empty: firstname", jsonObj["error"])
}

func TestUpdateUser(t *testing.T) {
	_, res := makeRequest("PUT", "/users/"+getUserID(), `{ "firstname": "Thea", "lastname": "Queen", "email": "thea@queen.com" }`)

	assert.Equal(t, http.StatusOK, res.Code)

	var user User
	getJSON(res, &user)

	assert.Equal(t, userID, user.ID)
	assert.Equal(t, "Thea", user.Firstname)
	assert.Equal(t, "Queen", user.Lastname)
	assert.Equal(t, "thea@queen.com", user.Email)
	assert.NotNil(t, user.Created)
	assert.NotNil(t, user.Updated)
}

func TestGetUsers(t *testing.T) {
	_, res := makeRequest("GET", "/users", "")

	assert.Equal(t, http.StatusOK, res.Code)

	var users []User
	getJSON(res, &users)

	assert.Equal(t, 1, len(users))
}

func TestGetInexistentUser(t *testing.T) {
	_, res := makeRequest("GET", "/users/0", "")

	assert.Equal(t, http.StatusNotFound, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "user [0] not found", jsonObj["error"])
}

func TestGetUser(t *testing.T) {
	_, res := makeRequest("GET", "/users/"+getUserID(), "")

	assert.Equal(t, http.StatusOK, res.Code)

	if res.Code == http.StatusOK {
		var user User
		getJSON(res, &user)

		assert.Equal(t, userID, user.ID)
		assert.Equal(t, "Thea", user.Firstname)
		assert.Equal(t, "Queen", user.Lastname)
		assert.Equal(t, "thea@queen.com", user.Email)
		assert.NotNil(t, user.Created)
		assert.NotNil(t, user.Updated)
	}
}

func TestDeleteInexsistentUser(t *testing.T) {
	_, res := makeRequest("DELETE", "/users/0", "")

	assert.Equal(t, http.StatusNotFound, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "user [0] not found", jsonObj["error"])
}

func TestDeleteUser(t *testing.T) {
	_, res := makeRequest("DELETE", "/users/"+getUserID(), "")

	assert.Equal(t, http.StatusNoContent, res.Code)

	jsonObj := getJSONMap(res)

	assert.Equal(t, "user ["+getUserID()+"] deleted", jsonObj["msg"])
}
